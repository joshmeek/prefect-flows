from prefect import Flow, task
from prefect.environments.storage import GitLab

@task
def do_i_exist():
    print("Yes")

with Flow("gitlab-test") as flow:
    do_i_exist()

flow.storage = GitLab(repo="joshmeek/prefect-flows", path="my_flow.py")
